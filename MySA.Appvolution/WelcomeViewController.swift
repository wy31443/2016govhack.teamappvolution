//
//  WelcomeViewController.swift
//  MySA.Appvolution
//
//  Created by wei yi on 30/07/2016.
//  Copyright © 2016 wei yi. All rights reserved.
//


import UIKit

var language = Int()

class WelcomeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBAction func goClicked(sender: AnyObject) {
        if(pickerData[picker.selectedRowInComponent(0)]==pickerData[0]){
            language = 0
        }else if(pickerData[picker.selectedRowInComponent(0)]==pickerData[1]){
            language = 1
        }else if(pickerData[picker.selectedRowInComponent(0)]==pickerData[2]){
            language = 2
        }else if(pickerData[picker.selectedRowInComponent(0)]==pickerData[3]){
            language = 3
        }else if(pickerData[picker.selectedRowInComponent(0)]==pickerData[4]){
            language = 4
        }
        
    }
    @IBOutlet weak var picker: UIPickerView!
    
    var pickerData: [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.picker.delegate = self
        self.picker.dataSource = self
        
        // Connect data:
        pickerData = ["English", "中文", "日本语", "Deutsch", "Français"]

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}