//
//  FirstViewController.swift
//  MySA.Appvolution
//
//  Created by wei yi on 30/07/2016.
//  Copyright © 2016 wei yi. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var text1: UILabel!
    @IBOutlet weak var nameTag: UILabel!
    @IBOutlet weak var ageTag: UILabel!
    @IBOutlet weak var hobbyTag: UILabel!
    @IBOutlet weak var budgetTag: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var picker: UIPickerView!
    var pickerData: [String] = ["500","1000","1500","2000","more"]
    
    @IBAction func nextClicked(sender: AnyObject) {
    }
    
    
    var EngWel: [String] = ["Welcome", "Your name", "Age", "Interests", "Budget", "Enjoy SA!"]
    var ChiWel: [String] = ["你好", "您的名字", "年龄", "兴趣", "您的预算?", "出发"]
    var JapWel: [String] = ["こんにちは", "名前", "年齢", "趣味", "一日過ごしたいんどのくらい?","行くぞ"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        self.picker.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
        if(language==0){
            text1.text=EngWel[0]
            nameTag.text=EngWel[1]
            ageTag.text=EngWel[2]
            hobbyTag.text=EngWel[3]
            budgetTag.text=EngWel[4]
            nextBtn.setTitle(EngWel[5], forState: .Normal)
        }else if (language==1){
            text1.text=ChiWel[0]
            nameTag.text=ChiWel[1]
            ageTag.text=ChiWel[2]
            hobbyTag.text=ChiWel[3]
            budgetTag.text=ChiWel[4]
            nextBtn.setTitle(ChiWel[5], forState: .Normal)
        }else if (language==2){
            text1.text=JapWel[0]
            nameTag.text=JapWel[1]
            ageTag.text=JapWel[2]
            hobbyTag.text=JapWel[3]
            budgetTag.text=JapWel[4]
            nextBtn.setTitle(JapWel[5], forState: .Normal)
        }else if (language==3){
            text1.text="Hallo"
        }else if (language==4){
            text1.text="Bonjour"
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }

}

